<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Real_Estate
 */

get_header(); ?>
<div class="container article-content">


      <div class="row details-content">
        <div class="col-xs-12">
          <div class="favorites-share clearfix">
            <div class="fav">
              <i class="fa fa-heart-o"></i>Add to favorites
            </div>
            <div class="share">
              <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <p>
            <?php 
             $id = get_the_ID();
              $post = get_page($id);
             echo $post->post_content; 
             
              ?>
          </p>
         
        </div>
      </div>
    
    
    <?php if( have_rows('add_new_paragraph') ): ?>
	<?php while( have_rows('add_new_paragraph') ): the_row(); 

		// vars
		$title = get_sub_field('paragraph_title');
		$subtitle = get_sub_field('paragraph_subtitle_despription');
		$description = get_sub_field('paragraph_description');
		$image = get_sub_field('paragraph_image');
		$quote = get_sub_field('paragraph_special_quote');

		?>
		
		
		  
      <div class="row">
       <?php if($image['url'] !=""):?>
        <div class="col-xs-12 col-sm-4">
          <img src="<?php echo $image['url'];?>" class="img-responsive center-block" alt="">
        </div>
        <div class="col-xs-12 col-sm-8">
         <?php endif;?>
          <div class="col-xs-12 heading mt-25">
          
           
             <?php if($title != ""):?>
            <h4><b><?php echo $title; ?></b></h4>
            <?php endif;?>
            <?php if($subtitle != ""):?>
            <p>
          <?php echo $subtitle; ?>
            </p>
            <?php endif; ?>
          </div>
          <?php if($description != ""): ?>
          <p>
            <?php echo $description; ?>
          </p>
          <?php endif;?>
          <?php if($quote != ""): ?>
           <div class="quote">
        <?php echo $quote ;?>
      </div>
       <?php endif;?>
       <?php if($image['url'] !=""):?>
        </div>
        <?php endif;?>
      </div>


	<?php endwhile; ?>

<?php endif; ?>
    
  
</div>
<?php
get_footer();
