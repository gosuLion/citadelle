<?php

class OurAchievements
{
    function displayAchievements()
    {
    ?>
    <div class="achievements">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <span class="sprite-load sprite-sold-properties"></span>
        <span class="count"><?php echo get_field('achievements_1');?></span>
        <p><?php _e('sold properties', 'real-estate');?></p>
      </div>
      <div class="col-xs-12 col-sm-4">
        <span class="sprite-load sprite-clients"></span>
        <span class="count"><?php echo get_field('achievements_2');?></span>
        <p><?php _e('clients', 'real-estate'); ?></p>
      </div>
      <div class="col-xs-12 col-sm-4">
        <span class="sprite-load sprite-agents"></span>
        <span class="count"><?php echo get_field('achievements_3');?></span>
        <p><?php _e('agents', 'real-estate'); ?></p>
      </div>
    </div>
  </div>
</div>
    <?php
    }
}