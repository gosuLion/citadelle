<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Real_Estate
 */

get_header(); ?>
<?php 
     get_template_part('estate_class/class', 'articles');
     $articles = new Articles();
     $articles->displayCategoryArticles(); 
     ?>

     
	 

<?php
get_footer();
