<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( ); ?>

   
    <?php
global $product ;
global $redux_demo;
        //Get the attributes
       $neighbourName = $redux_demo['product-neighbourhood'];
       $areaName = $redux_demo['product-area'];
        $locationName = $redux_demo['product-property'];
        $bedroomName = $redux_demo['product-bedrooms'];
        $bathroomName = $redux_demo['product-bathroom'];
        $rentName = $redux_demo['product-rent'];
        $soldName = $redux_demo['product-sold'];
        $saleName = $redux_demo['product-sale'];

            

        //Get product attributes
            $bathrooms = wp_get_post_terms( $post->ID, $bathroomName);
            $bedrooms = wp_get_post_terms( $post->ID, $bedroomName);
            $rent = wp_get_post_terms( $post->ID, $rentName);
            $sale = wp_get_post_terms( $post->ID, $saleName);
            $sold = wp_get_post_terms( $post->ID, $soldName);
            $nneighbour = wp_get_post_terms( $post->ID, $neighbourName);
            $areaSize = wp_get_post_terms( $post->ID, $areaName);
            $locationArea = wp_get_post_terms( $post->ID, $locationName);


 //Get the currency

    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
    //Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
    //Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
    $salePrice = $salePrice  * $_SESSION['price'];
   //Product SKU
    $sku = get_post_meta(get_the_ID());
//Get and split the short description to 190 characters
    $shortDescription = str_split(get_the_excerpt(),1000);
?>
        <!-- description: START -->
        <div class="container description">
              <?php

// Adding Breadcrumbs by Yoast


if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<div class="breadcrumbs"><p>','</p></div>
');
}
?>
            <div class="row">
                <!-- left description: START -->
                <div class="col-xs-12 col-md-8">
                    <div class="ribbon">
                        <?php if($rent[0]->name !="")
{
            echo _e('For Rent', 'real-estate');
}
              else if($sale[0]->name !="")
              {
                  echo _e('For Sale', 'real-estate');
              }
              else if($sold[0]->name !="")
              {
                  echo _e('Sold', 'real-estate');
              }
              ?>
                    </div>
                    <div class="favorites-share">
                       
                          
                           
                           <div class="fav">
                            <!-- <i class="fa fa-heart-o"></i>Add to favorites -->
                        </div>
                        <div class="share">
                          <!--  <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a> -->
                        </div>
                    </div>
                    <h2><b><?php echo get_the_title(); ?></b></h2>
                    <div class="id-price clearfix">
                    
                       <?php if($price !="" || $sale !=""): ?>
        
                        <div class="price"><b><?php echo $currency ; ?> <?php if($salePrice !=""){
                        echo number_format($salePrice, $numberOfDecimals,".",",") ;
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                            
                      ?></b></div>
                       <?php endif; ?>
                        <div class="id clearfix">
                           <?php
                            
                            if($sku[_sku][0]) :?>
                            <p>
                                <?php _e('Property ID:', 'real-estate'); ?> <b><?php echo $sku[_sku][0]; ?></b> </p>
                            <p class="small"></p>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="text">
                        <?php echo $shortDescription[0]; ?> <a data-toggle="collapse" class="btn" data-target="#property-description"> [ read more ]</a>
                            <div id="property-description" class="collapse">
                                <?php
                for($i=1 ; $i < count($shortDescription); $i++)
                {
                echo $shortDescription[$i]." ";
                }?>
                            </div>
                    </div>
                    <div class="specs">
                       <?php if($bedrooms[0]->name) :?>
                        <span class="sprite-load ico-bdr"></span>
                        <?php
                if($bedrooms[0]->name == 1){
                    echo $bedrooms[0]->name. _(' bedroom', 'real-estate');
                }
                else
                {
                    echo $bedrooms[0]->name._(' bedrooms', 'real-estate');
                }
                    ?>
                            <?php endif;?>
                            <?php if($bathrooms[0]->name) :?>
                            <span class="sprite-load ico-bth"></span>
                            <span><?php
                if($bathrooms[0]->name == 1){
                    echo $bathrooms[0]->name._('bathroom', 'real-estate');
                }
                else
                {
                    echo $bathrooms[0]->name._('bathrooms', 'real-estate');
                }
                    ?>
                    <?php endif;?>
                    <?php if($areaSize[0]->name) :?>
            <span class="sprite-load ico-size"></span>
                            <span><?php echo $areaSize[0]->name * $_SESSION['change']; ?>
                             <?php if($_SESSION['change'] == 1){
                    echo "m2";
}
                                else
                                {
                                    echo "sq ft";
                                }
                              ;?></span>
                            <?php endif;?>
                            <br class="mobile-show">
                            <br class="mobile-show">
                            <?php if(get_field('product_garage')):?>
                            <span class="sprite-load ico-prk"></span>
                            <span><?php echo get_field('product_garage'); ?> parkings space</span>
                            <?php endif;?>
                            <?php if(get_field('product_pool_size')):?>
                            <span class="sprite-load ico-pool"></span>
                            <span>Outdoor pool</span>
                            <?php endif;?>
                    </div>
                </div>
                <!-- left description: END -->
                <!-- right description: START -->
                   <?php
     get_template_part('estate_class/class', 'ouragents');
     $agents = new OurAgents();
     $agents->displayAgent();
     ?>
                <!-- right description: END -->
            </div>
        </div>
        <!-- description: END -->

        <!-- tabbed content: START -->

        <div class="real-estate-tabbed">
            <div class="container">
                <div class="row">
                    <!-- tab head START -->
                    <ul class="nav nav-pills">
                        <li class="col-lg-2 active" role="presentation">
                            <a data-toggle="tab" href="#address">
                                <?php _e('Address / Location', 'real-estate');?>
                            </a>
                        </li>
                        <li class="col-lg-2" role="presentation">
                            <a data-toggle="tab" href="#details">
                                <?php _e('Details', 'real-estate');?>
                            </a>
                        </li>
                        <li class="col-lg-2" role="presentation">
                            <a data-toggle="tab" href="#features">
                                <?php _e('Features' , 'real-estate');?>
                            </a>
                        </li>
                        <li class="col-lg-2" role="presentation">
                            <a data-toggle="tab" href="#plans">
                                <?php _e('Plans' , 'real-estate');?>
                            </a>
                        </li>
                        <li class="col-lg-2" role="presentation">
                            <a data-toggle="tab" href="#gallery">
                                <?php _e('Gallery', 'real-estate');?>
                            </a>
                        </li>
                    </ul>
                    <!-- tab head END -->

                    <!-- tabs content: START -->
                    <div class="tab-content col-xs-12 clearfix">

                        <div class="tab-pane active clearfix" id="address">
                            <div class="col-xs-6 col-sm-3">
                                <ul class="plain">
                                   <?php if(get_field('product_address')):?>
                                    <li>
                                        <span class="attr"><b><?php _e('Address', 'real-estate');?></b></span>:
                                        <?php echo get_field('product_address')['address'];?>
                                    </li>
                                    <?php endif;?>
                                    <?php if(get_field('product_city')):?>
                                    <li>
                                        <span class="attr"><b><?php _e('City', 'real-estate');?></b></span>:
                                        <?php echo get_field('product_city');?>
                                    </li>
                                    <?php endif;?>
                                    <?php if(get_field('product_county')):?>
                                    <li>
                                        <span class="attr"><b><?php _e('State/County', 'real-estate');?></b></span>:
                                        <?php echo get_field('product_county');?>
                                    </li>
                                    <?php endif;?>
                                </ul>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <ul class="plain">
                                   <?php if(get_field('product_zip')):?>
                                    <li>
                                        <span class="attr"><b><?php _e('Zip', 'real-estate');?></b></span>/
                                        <?php _e('Postal Code:', 'real-estate');?>
                                            <?php echo get_field('product_zip');?>
                                    </li>
                                    <?php endif;?>
                                    <?php if($nneighbour[0]->name):?>
                                    <li>
                                        <span class="attr"><b><?php _e('Neighborhood', 'real-estate');?></b></span>:
                                        <?php echo $nneighbour[0]->name;?>
                                    </li>
                                    <?php endif;?>
                                    <?php if(get_field('product_country')):?>
                                    <li>
                                        <span class="attr"><b><?php _e('Country', 'real-estate'); ?></b></span>:
                                        <?php echo get_field('product_country');?>
                                    </li>
                                    <?php endif;?>
                                </ul>
                            </div>
                            <!-- map and pins with adress, center and pin image options -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- map pins -->
                                    
                                  <?php 

$location = get_field('product_address');
if( !empty($location) ):
?>

<div class="acf-map mmap">
	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
</div>
<?php endif; ?>
                                </div>
                            </div>
                            <!-- end map -->
                        </div>

                        <div class="tab-pane" id="details">
                            <div class="row">
                                <div class="col-xs-6 col-sm-3">
                                    <ul class="plain">
                                       <?php if($sku[_sku][0]):?>
                                        <li>
                                            <span class="attr"><b> <?php _e('Property ID', 'real-estate'); ?></b></span>:
                                            <?php echo $sku[_sku][0];?>
                                        </li>
                                        <?php endif;?>
                                        <?php if($price || $sale): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Price', 'real-estate');?></b></span>:
                                            <?php echo $currency ; ?>
                                                <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",") ;
                      }
                      ?>
                                        </li>
                                        <?php endif;?>
                                        <?php if($areaSize[0]->name): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Property Size' , 'real-estate');?></b></span>:
                                           <?php echo $areaSize[0]->name * $_SESSION['change']; ?>
                             <?php if($_SESSION['change'] == 1){
                    echo "m2";
}
                                else
                                {
                                    echo "sq ft";
                                }
                              ;?>
                                        </li>
                                        <?php endif;?>
                                        <?php if($bedrooms[0]->name): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Bedrooms', 'real-estate');?></b></span>:
                                            <?php echo $bedrooms[0]->name; ?>
                                        </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <ul class="plain">
                                        <?php if($bathrooms[0]->name): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Bathrooms', 'real-estate');?></b></span>:
                                            <?php echo $bathrooms[0]->name; ?>
                                        </li>
                                        <?php endif;?>
                                        <?php if(get_field('product_garage')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Garage', 'real-estate'); ?></b></span>:
                                            <?php echo get_field('product_garage'); ?>
                                        </li>
                                       
                                        <li>
                                            <span class="attr"><b><?php _e('Garage Size', 'real-estate');?></b></span>:
                                            <?php echo get_field('product_garage_size'); ?> <?php if($_SESSION['change'] == 1){
                    echo "m2";
}
                                else
                                {
                                    echo "sq ft";
                                }
                              ;?>
                                        </li>
                                        <?php endif;?>
                                         <?php if(get_field('product_year_built')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Year Built', 'real-estate'); ?></b></span>:
                                            <?php echo get_field('product_year_built'); ?>
                                        </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <ul class="plain">
                                       <?php if(get_field('product_deposit')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Deposit', 'real-estate');?></b></span>:
                                            <?php echo get_field('product_deposit'); ?>%
                                        </li>
                                        <?php endif; ?>
                                        
                                        <?php if(get_field('product_pool_size')) : ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Pool Size' , 'real-estate');?></b></span>:
                                            <?php echo get_field('product_pool_size'); ?>  <?php if($_SESSION['change'] == 1){
                    echo "m2";
}
                                else
                                {
                                    echo "sq ft";
                                }
                              ;?>
                                        </li>
                                        <?php endif;?>
                                        <?php if (get_field('product_last_remodel_year')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Last remodel year', 'real-estate') ;?></b></span>:
                                            <?php echo get_field('product_last_remodel_year'); ?>
                                        </li>
                                        <?php endif;?>
                                         <?php if (get_field('product_amenities')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Amenities', 'real-estate') ;?></b></span>:
                                            <?php echo get_field('product_amenities'); ?>
                                        </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <ul class="plain">
                                       <?php if(get_field('product_aditional_rooms')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Additional Rooms', 'real-estate');?></b></span>:
                                            <?php echo get_field('product_aditional_rooms'); ?>
                                        </li>
                                        <?php endif;?>
                                        <?php if(get_field('product_equipment')): ?>
                                        <li>
                                            <span class="attr"><b><?php _e('Equipment', 'real-estate');?></b></span>:
                                            <?php echo get_field('product_equipment'); ?>
                                        </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 mt-20">
                                    <p>
                                        <?php 
                    echo $post->post_content;?>
                                    </p>
                                </div>
                            </div>
                        </div>


                        <?php if( have_rows('product_floor') ): ?>
                            <div class="tab-pane" id="features">

                                <?php while( have_rows('product_floor') ): the_row(); 

		// vars
		$name = get_sub_field('product_floor_name');
		?>
                                    <div class="row">

                                        <?php $i = 0; ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <?php if($i == 0):?>
                                                    <h4><b><?php echo $name; ?></b></h4>
                                                    <br>
                                                    <?php endif;?>
                                            </div>
                                            <?php if( have_rows('product_features') ): ?>
                                                <?php while( have_rows('product_features') ): the_row(); 

		// vars
		$feature = get_sub_field('add_features');
		
		?>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <ul class="plain">
                                                            <li><i class="fa fa-check" aria-hidden="true"></i>
                                                                <?php echo $feature; ?>
                                                            </li>
                                                        </ul>

                                                    </div>
                                                    <?php $i++; ?>
                                                        <?php endwhile; ?>

                                                            <?php endif; ?>

                                    </div>
                                    <br>
                                    <?php endwhile; ?>
                            </div>
                            <?php endif; ?>



                                <div class="tab-pane" id="plans">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <ul class="nav nav-pills nav-stacked">
                                                <?php if( have_rows('product_plans') ): ?>
                                                    <?php $i = 0 ; ?>
                                                        <?php while( have_rows('product_plans') ): the_row(); 

		// vars
		$name = get_sub_field('produc_floor_name');
		$ref = get_href($name);
		?>

                                                            <li <?php if($i==0 ):?>
                                                                class="active"
                                                                <?php endif; ?>
                                                                    ><a data-toggle="tab" href="#<?php echo $ref;?>"><b><?php echo $name; ?></b><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

                                                            <?php $i++; ?>
                                                                <?php endwhile; ?>

                                                                    <?php endif; ?>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-sm-7 col-sm-offset-1">
                                            <div class="tab-content col-xs-12 clearfix">
                                                <?php if( have_rows('product_plans') ): ?>

                                                    <?php $i = 0; ?>
                                                        <?php while( have_rows('product_plans') ): the_row(); 

		// vars
        $name = get_sub_field('produc_floor_name');
		$ref = get_href($name);
		$image = get_sub_field('product_floor_image');
		?>
                                                            <div class="tab-pane <?php if($i == 0):?>
        active
        <?php endif; ?> clearfix" id="<?php echo $ref;?>">
                                                                <img src="<?php echo $image['url'];?>" class="img-responsive center-block" alt="<?php echo $image['alt'];?>">
                                                            </div>

                                                            <?php $i++;?>
                                                                <?php endwhile; ?>

                                                                    <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="gallery">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="connected-carousels">
                                                <div class="stage">
                                                    <div class="carousel carousel-stage">
                                                        <ul>
                                                            <?php
    $images = get_field('image_gallery');
if( $images ): ?>

                                                                <?php foreach( $images as $image ): ?>

                                                                    <li><img src="<?php echo $image['url'];?>" class="img-responsive center-block" alt="<?php echo $image['alt'];?>"></li>

                                                                    <?php endforeach; ?>

                                                                        <?php endif; ?>
                                                                            <?php if( have_rows('video_gallery') ): ?>


                                                                                <?php while( have_rows('video_gallery') ): the_row(); 

		// vars
		$video = get_sub_field('add_video');
		?>
                                                                                    <li>
                                                                                        <iframe src="<?php echo esc_url($video); ?>" class="img-responsive" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                                                    </li>

                                                                                    <?php endwhile; ?>

                                                                                        <?php endif; ?>


                                                        </ul>
                                                    </div>
                                                    <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
                                                    <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
                                                </div>

                                                <div class="navigation">
                                                    <a href="#" class="prev prev-navigation">&lsaquo;</a>
                                                    <a href="#" class="next next-navigation">&rsaquo;</a>
                                                    <div class="carousel carousel-navigation">
                                                        <ul>
                                                            <?php
    $images = get_field('image_gallery');
if( $images ): ?>

                                                                <?php foreach( $images as $image ): ?>

                                                                    <li><img src="<?php echo $image['url'];?>" class="img-responsive center-block" alt="<?php echo $image['alt'];?>">
                                                                        <div class="overlay">
                                                                        </div>
                                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                                    </li>

                                                                    <?php endforeach; ?>

                                                                        <?php endif; ?>

                                                                            <?php if( have_rows('video_gallery') ): ?>


                                                                                <?php while( have_rows('video_gallery') ): the_row(); 

		// vars
		$video = get_sub_field('add_video_image');
		?>
                                                                                    <li><img src="<?php echo $video['url'];?>" class="img-responsive center-block" alt="<?php echo $video['alt'];?>">
                                                                                        <div class="overlay">
                                                                                        </div>
                                                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                                                    </li>

                                                                                    <?php endwhile; ?>

                                                                                        <?php endif; ?>


                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                    <!-- tabs content: START -->
                </div>
            </div>
        </div>
        <!-- tabbed content: END -->
 

        <!-- recently added: START -->
        <?php
     get_template_part('estate_class/class', 'realestate');
     $recently = new RealEstate();
     $recently->displayRecently();
     ?>
            <!-- recently added: END -->


            <?php get_footer(); ?>