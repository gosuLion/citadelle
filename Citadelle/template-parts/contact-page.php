<?php

/**
 * Template Name: Contact Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header();
global $redux_demo;
?>


<div class="container">
      <div class="row">
        <div class="col-xs-12 title">
          <h2><b><?php the_title();?></b></h2>
          <p>
           <?php echo get_field('subtitle_description');?>
          </p>
          <div class="line"></div>
        </div>
      </div>

      <div class="row contact-content">
        <div class="col-xs-12 col-sm-6">
          <div class="title">
            <h3><b><?php _e('GET IN TOUCH WITH THE BEST AGENTS!'); ?></b></h3>
            <div class="line"></div>
          </div>
          <p>
           <?php echo get_field('description_area');?>
          </p>
          <div class="details">
            <h4><b><?php echo get_field('website_title');?></b></h4>
            <p><b><i class="fa fa-map-marker"></i> <?php echo get_field('address_header', 'option');?></b></p>
            <p><b><a href="tel:<?php 
        
        if($redux_demo['phone-header'] == ""){
            echo "0800 800 800";
        }
        else
        {
        echo $redux_demo['phone-header']; 
        }?>"><i class="fa fa-phone"></i> <?php 
        
        if($redux_demo['phone-header'] == ""){
            echo "0800 800 800";
        }
        else
        {
        echo $redux_demo['phone-header']; 
        }?></a></b></p>
            <p><b><a href="mailto:<?php 
        
        if($redux_demo['phone-header'] == ""){
            echo "contact@citadelle.com";
        }
        else
        {
        echo $redux_demo['phone-header']; 
        }?>"><i class="fa fa-envelope-o"></i> <?php 
        
        if($redux_demo['email-header'] == ""){
            echo "contact@citadelle.com";
        }
        else
        {
        echo $redux_demo['email-header']; 
        }?></a></b></p>
            <p class="social">
              <?php 
               
            $navigation = new Navigation();
                $navigation->displayFooterSocials(); ?>
            </p>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6">
          <form action="" id="contact-form" class="form-contact"> <!-- simple contact form: START -->
           <?php echo do_shortcode('[contact-form-7 id="151" title="Contact form 1"]'); ?>
          </form> <!-- simple contact form: END -->
        </div>
      </div>
    </div>

<?php

get_footer();