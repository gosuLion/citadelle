<?php

get_header();
/**
 * Template Name: Typografy Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */
?>
<!-- content: START -->
<div class="content">
  <div class="container">

    <!-- colors: START -->
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3"> <div class="color one">#383f48 <br><span class="rgb">RGB 56/63/72</span></div></div>
      <div class="col-xs-12 col-sm-6 col-md-3"><div class="color two">#0088cc <br><span class="rgb">RGB 0/136/204</span></div></div>
      <div class="col-xs-12 col-sm-6 col-md-3"><div class="color three">#2baab1 <br><span class="rgb">RGB 543/170/177</span></div></div>
      <div class="col-xs-12 col-sm-6 col-md-3"><div class="color four">#e36159 <br><span class="rgb">RGB 227/97/89</span></div></div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6"><div class="color-two one">#999999</div></div>
      <div class="col-xs-12 col-sm-6"><div class="color-two two">#f8f8f8</div></div>
    </div>
    <!-- colors: END -->

    <!-- headlines: START -->
    <div class="row headings">
      <div class="col-xs-12 col-sm-3">
        <h1 class="h1"><b>Headline 1</b></h1>
      </div>
      <div class="col-xs-12 col-sm-3">
        <h2 class="h2"><b>Headline 2</b></h2>
      </div>
      <div class="col-xs-12 col-sm-3">
        <h3 class="h3"><b>Headline 3</b></h3>
      </div>
    </div>
    <!-- headlines: END -->

    <!-- paragraph types: START -->
    <div class="row">
      <div class="col-xs-12">
        <p class="primary-p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est erat, interdum non lacus at, vulputate bibendum tortor. Suspendisse eu nisi lacus. 
          Ut a venenatis mi, eget accumsan leo. Cras id mauris orci. Cras sodales sed dui et venenatis. Mauris et eleifend felis. Nunc turpis lectus, luctus ut velit 
          nec, tempus malesuada magna. Suspendisse augue arcu, aliquam vitae orci vestibulum, rhoncus commodo massa. Praesent sodales dictum ultricies. 
          Proin at leo vel neque vestibulum aliquam id vitae lectus. Proin ultricies erat semper risus aliquet viverra. Phasellus dignissim pulvinar sollicitudin.
        </p>
        <p class="secondary-p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est erat, interdum non lacus at, vulputate bibendum tortor. Suspendisse eu nisi lacus. 
          Ut a venenatis mi, eget accumsan leo. Cras id mauris orci. Cras sodales sed dui et venenatis. Mauris et eleifend felis. Nunc turpis lectus, luctus ut velit 
          nec, tempus malesuada magna. Suspendisse augue arcu, aliquam vitae orci vestibulum, rhoncus commodo massa. Praesent sodales dictum ultricies. 
          Proin at leo vel neque vestibulum aliquam id vitae lectus. Proin ultricies erat semper risus aliquet viverra. Phasellus dignissim pulvinar sollicitudin.
        </p>
        <p class="third-p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est erat, interdum non lacus at, vulputate bibendum tortor. Suspendisse eu nisi lacus. 
          Ut a venenatis mi, eget accumsan leo. Cras id mauris orci. Cras sodales sed dui et venenatis. Mauris et eleifend felis. Nunc turpis lectus, luctus ut velit 
          nec, tempus malesuada magna. Suspendisse augue arcu, aliquam vitae orci vestibulum, rhoncus commodo massa. Praesent sodales dictum ultricies. 
          Proin at leo vel neque vestibulum aliquam id vitae lectus. Proin ultricies erat semper risus aliquet viverra. Phasellus dignissim pulvinar sollicitudin.
        </p>
        <p class="small-p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est erat, interdum non lacus at, vulputate bibendum tortor. Suspendisse eu nisi lacus. 
          Ut a venenatis mi, eget accumsan leo. Cras id mauris orci. Cras sodales sed dui et venenatis. Mauris et eleifend felis. Nunc turpis lectus, luctus ut velit 
          nec, tempus malesuada magna. Suspendisse augue arcu, aliquam vitae orci vestibulum, rhoncus commodo massa. Praesent sodales dictum ultricies. 
          Proin at leo vel neque vestibulum aliquam id vitae lectus. Proin ultricies erat semper risus aliquet viverra. Phasellus dignissim pulvinar sollicitudin.
        </p>
      </div>
    </div>
    <!-- paragraph types: END -->

    <!-- buttons: START -->
    <div class="row">
      <div class="col-xs-12 col-sm-3">
        <a href="" class="btn-secondary">BUTTON 1</a>
      </div>
      <div class="col-xs-12 col-sm-3">
        <a href="" class="btn-third">BUTTON 2</a>
      </div>
      <div class="col-xs-12 col-sm-3">
        <a href="" class="btn-fourth">BUTTON 3</a>
      </div>
    </div>
    <!-- buttons: END -->

    <!-- list: START -->
    <div class="row">
      <div class="col-xs-12">
        <ul class="plain list">
          <li><i class="fa fa-check"></i> Praesent vehicula a ipsum vitae consequat. </li>
          <li><i class="fa fa-check"></i> Phasellus a congue lacus. Sed a erat ac quam mattis condimentum.</li>
          <li><i class="fa fa-check"></i> Suspendisse metus sem, sagittis sed odio at, finibus faucibus justo.</li>
          <li><i class="fa fa-check"></i> Praesent ac leo metus. </li>
          <li><i class="fa fa-check"></i> Aliquam laoreet arcu ut metus porta cursus.</li>
        </ul>
      </div>
    </div>
    <!-- list: END -->

    <!-- quote: START -->
    <div class="row">
      <div class="quote">
        Highlighted text or quotes - ‘Lorem ipsum dolor sit metus ac leo vitae duis sem eu est.’
      </div>
    </div>
    <!-- quote: END -->

    <!-- section 1: START -->
    <div class="row">
      <div class="col-xs-12 heading">
        <h4><b>SECTION 1</b></h4>
        <p>
          Nam fermentum rutrum turpis, vitae pulvinar metus lobortis eu. Nulla facilisis ac tortor ut luctus. Duis vitae tincidunt nunc. Duis faucibus, nibh at euismod feugiat, nunc neque feugiat nisi, nec finibus felis sem eu est. 
        </p>
      </div>
    </div>
    <!-- section 1: END -->

    <!-- section 2: START -->
    <div class="row">
      <div class="col-xs-12 col-md-4">
        <img src="http://placehold.it/380x280.jpg" class="img-responsive center-block" alt="">
      </div>
      <div class="col-xs-12 col-md-8">
        <div class="col-xs-12 heading mt-25">
          <h4><b>SECTION 2</b></h4>
          <p>
            Nam fermentum rutrum turpis, vitae pulvinar metus lobortis eu. Nulla facilisis ac tortor ut luctus. Duis vitae tincidunt <br class="mobile-hide">
            Duis faucibus, nibh at euismod feugiat, nunc neque feugiat nisi, nec finibus felis sem eu est. 
          </p>
        </div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean est erat, interdum non lacus at, 
          vulputate bibendum tortor. Suspendisse eu nisi lacus. Ut a venenatis mi, eget accumsan leo. Cras 
          id mauris orci. Cras sodales sed dui et venenatis. Mauris et eleifend felis. Nunc turpis lectus, lucts 
          ut velit nec, tempus malesuada magna. Suspendisse augue arcu, aliquam vitae orci vestibulum, 
          commodo massa. Praesent sodales dictum ultricies. 
        </p>
      </div>
    </div>
    <!-- section 2: END -->




    <!-- tabbed : START -->
    <div class="row">
      <div class="real-estate-tabbed">

        <!-- tab head START -->
        <ul class="nav nav-pills">
          <li class="active" role="presentation">
            <a data-toggle="tab" href="#tab-1">TAB 1</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#tab-2">TAB 2</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#tab-3">TAB 3</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#tab-4">TAB 4</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#tab-5">TAB 5</a>
          </li>
        </ul>
        <!-- tab head END -->

        <!-- tabs content: START -->
        <div class="tab-content clearfix">

          <div class="tab-pane active clearfix" id="tab-1">
            Tab 1 content
          </div>

          <div class="tab-pane" id="tab-2">
            Tab 2 content
          </div>

          <div class="tab-pane" id="tab-3">
            Tab 3 content
          </div>

          <div class="tab-pane" id="tab-4">
            Tab 4 content
          </div>

          <div class="tab-pane market-reports" id="tab-5">
            Tab 5 content
          </div>
        </div>
        <!-- tabs content: END -->

      </div>
    </div>
    <!-- tabbed: END -->

    <!-- map pins: START -->
    <div class="row pins">
      <div class="col-xs-12 col-sm-2">
        <p class="primary-p">Map Pins</p>
      </div>
      <div class="col-xs-12 col-sm-8">
        <img src="../wp-content/themes/Citadelle/assets/images/sold-pin.png" alt="">
        <img src="../wp-content/themes/Citadelle/assets/images/for-rent-pin.png" alt="">
        <img src="../wp-content/themes/Citadelle/assets/images/for-sale-pin.png" alt="">

        <br class="mobile-show">

        <img src="../wp-content/themes/Citadelle/assets/images/sold-pin-big.png" alt="">
        <img src="../wp-content/themes/Citadelle/assets/images/for-rent-pin-big.png" alt="">
        <img src="../wp-content/themes/Citadelle/assets/images/for-sale-pin-big.png" alt="">
      </div>
    </div>
    <!-- map pins: END -->
    <br>
    <!-- map pins: START -->
    <div class="row facilities-icons">
      <div class="col-xs-12 col-sm-2">
        <p class="primary-p">Custom Icons</p>
      </div>
      <div class="col-xs-12 col-sm-8">
        <i class="sprite-load ico-bedroom"></i>
        <i class="sprite-load ico-bathroom"></i>
        <i class="sprite-load ico-parking"></i>
        <i class="sprite-load ico-size"></i>
        <i class="sprite-load ico-pool"></i>
        <i class="sprite-load"></i>
      </div>
    </div>
    <!-- map pins: END -->
    
    <!-- faq expandables: START -->
    <div class="row">
      <div class="col-xs-12">
        <!--   -->
        <div class="faq-expand expand">
          <h2 data-toggle="collapse" class="btn-expand" data-target="#expand-1">
            <i class="fa fa-minus"></i>
            <b>QUESTION 1</b>
          </h2>
          <div id="expand-1" aria-expanded="true" role="tree" class="collapse in expand-content">
            <p> 
              Morbi nec metus neque. Ut non lectus eget leo pharetra pretium eu scelerisque lorem. Vivamus vel elit quis enim maximus vehicula. Quisque 
              gravida erat non justo lacinia scelerisque. Sed a erat magna. Curabitur laoreet varius enim id rhoncus. Sed vitae ante eget orci aliquet semper ut 
              at sem. Nulla blandit nibh id sapien congue, eget aliquet felis ultrices. Phasellus non velit a risus bibendum ullamcorper. Vivamus vel bibendum 
              elit, nec rhoncus mi. Vestibulum ut felis eu nisl sagittis finibus quis quis dolor.
            </p>
          </div>
        </div>
        <!--   -->
        <div class="faq-expand expand">
          <h2 data-toggle="collapse" class="btn-expand" data-target="#expand-2">
            <i class="fa fa-plus"></i>
            <b>QUESTION 2</b>
          </h2>
          <div id="expand-2" aria-expanded="false" role="tree" class="collapse expand-content">
            <p> 
              Morbi nec metus neque. Ut non lectus eget leo pharetra pretium eu scelerisque lorem. Vivamus vel elit quis enim maximus vehicula. Quisque 
              gravida erat non justo lacinia scelerisque. Sed a erat magna. Curabitur laoreet varius enim id rhoncus. Sed vitae ante eget orci aliquet semper ut 
              at sem. Nulla blandit nibh id sapien congue, eget aliquet felis ultrices. Phasellus non velit a risus bibendum ullamcorper. Vivamus vel bibendum 
              elit, nec rhoncus mi. Vestibulum ut felis eu nisl sagittis finibus quis quis dolor.
            </p>
          </div>
        </div>
        <!--   -->

      </div>
    </div>
    <!-- faq expandables: END -->

  </div>
</div>



<!-- content: END -->
<?php
get_footer();