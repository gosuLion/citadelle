<?php

/**
 * Template Name: Account Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>

<div class="container">
  <?php if(!is_user_logged_in()) :?>
   <div class="row">
    <div class="col-md-6">
        <?php echo do_shortcode('[wppb-login]'); ?>
        <br>
        <?php do_shortcode('[oa_social_login]'); ?>
    </div>
    <div class="col-md-6">
        <?php echo do_shortcode('[wppb-register]'); ?>
    </div>
</div>

<?php endif;?>
<?php if(is_user_logged_in()) :?>
 <div class="row">
    <div class="col-md-12">
       <?php echo do_shortcode('[wppb-edit-profile]'); ?>
    </div>
</div>
<?php endif;?>
</div>
<?php
get_footer();