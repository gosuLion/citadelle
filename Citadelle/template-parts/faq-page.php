<?php

/**
 * Template Name: FAQ Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>
 <div class="container faq-content" id="toScroll">
    <div class="row">
      <div class="col-xs-12">

       <?php
          $faq = new Faq();
          $faq->displayQuestions();?>

      </div>
    </div>
  </div>

<?php
get_footer();