<?php

/**
 * Template Name: Gallery Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>

<!-- photo gallery -->
 <?php
     get_template_part('estate_class/class', 'photogallery');
     $gallery = new PhotoGallery();
     $gallery->displayGallery();
     ?>
<!-- end photo gallery -->

<?php
get_footer();