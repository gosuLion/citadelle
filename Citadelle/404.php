<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Real_Estate
 */

get_header();
global $redux_demo;
?>
<div class="content">
   
    <img src="<?php echo $redux_demo['404-image']['url']; ?>" class="img-responsive center-block" alt="">
    <p>
      <b>
        Oh! Looks like this building is a bit rusty. <br class="mobile-hide">Let’s search something better.
      </b>
    </p>
    <a href="<?php echo home_url(); ?>" class="btn-secondary">I’d like that</a>
    <p>
      <b>
        or you can go back in the previous page  <br class="mobile-hide">and try again
      </b>
    </p>
    <a href="<?php echo home_url(); ?>" class="btn-primary">Take me back</a>
  </div>

<?php
get_footer();
